//
//  MoviesViewController.swift
//  MovieTest
//
//  Created by Shahzaib Maqbool on 20/04/2019.
//  Copyright © 2019 Shahzaib Maqbool. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage
class Connectivity
{
    class var isConnectedToInternet:Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
}
class MoviesViewController: UIViewController
{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableVIew: UITableView!
    var movieId: String?
    let serviceHelper = ServiceHelper()
    var popularMoviesData: PopularMovies?
    var popularMoviesResult: [PopularMoviesResult] = []
    var sarchedPopMoviesResult: [PopularMoviesResult] = []
    override func viewDidLoad()
    {
        super.viewDidLoad()
        searchBar.delegate = self
        self.getPopularMovies()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismisKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    //Calls this function when the tap is recognized.
    @objc func dismisKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    var isKeyboardAppear = false
    @objc func keyboardWillShow(notification: NSNotification)
    {
        if !isKeyboardAppear
        {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            {
                if self.searchBar.frame.origin.y == Config.SCREEN_HEIGHT - 56
                {
                    self.searchBar.frame.origin.y -= keyboardSize.height
                }
            }
            isKeyboardAppear = true
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        if isKeyboardAppear
        {
            if self.searchBar.frame.origin.y != 0
            {
                self.searchBar.frame.origin.y = Config.SCREEN_HEIGHT - 56
            }
            isKeyboardAppear = false
        }
    }
    ////for getting all popular movies
    func getPopularMovies()
    {
        if Connectivity.isConnectedToInternet
        {
            AppSettings().showSpinner()
            
            serviceHelper.getPopularMovies( { (movies) in
                print("Success")
                self.popularMoviesData = movies
                self.popularMoviesResult = movies.results!
                self.sarchedPopMoviesResult = movies.results!
                self.tableVIew.reloadData()
                AppSettings().dismissSpinner()
               
                
            })
            { (error) in
                BackBoneViewController.petrichors_showErrorMsgOnStatusBar(title: "Error!".localized, desc: "No more results.".localized)
                print("Error")
            }
        }
        else
        {
            BackBoneViewController.petrichors_showErrorMsgWithTitle(title: "Error!".localized, desc: "No network connection".localized)
        }
    }

  

}
//extension MoviesViewController: UITableViewDelegate
//{
//
//}
/////tableview handling methods
extension MoviesViewController: UITableViewDataSource,UISearchBarDelegate,UITableViewDelegate
{
   
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.popularMoviesResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "moviesCell") as! UserTableViewCell

        
        cell.fullNameLabel?.text = "\(self.popularMoviesResult[indexPath.row].title ?? "")"

        if self.popularMoviesResult[indexPath.row].backdrop_path != nil
        {
            if let urlString = self.popularMoviesResult[indexPath.row].backdrop_path
            {
                
                cell.image2?.sd_setImage(with: URL(string: "\(Config.IMAGE_BASE_URL)\(urlString)"), placeholderImage: UIImage(named: "news_placeholder"))
                
            }
            else
            {
                cell.image2?.image = #imageLiteral(resourceName: "news_placeholder")
            }
        }
        else
        {
            cell.image2?.image = #imageLiteral(resourceName: "news_placeholder")
        }
        return cell
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segue_moviesVC_toDetailVC"
        {
            if let vc = segue.destination as? MovieDetailViewController
            {
                //vc.md5ProductIdFromDashboard = self.md5ProductId?.md5()
                vc.movieId = self.movieId
            }
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.movieId = "\(self.popularMoviesResult[indexPath.row].id ?? 0)"
        self.performSegue(withIdentifier: "segue_moviesVC_toDetailVC", sender: self)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.count > 0
        {
            popularMoviesResult = sarchedPopMoviesResult.filter({($0.title?.lowercased().contains(searchText.lowercased()))!})
        }
        else
        {
            popularMoviesResult = sarchedPopMoviesResult
        }
        
        // searching = true
        tableVIew.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //searching = false
        searchBar.text = ""
        view.endEditing(true)
        popularMoviesResult = sarchedPopMoviesResult
        tableVIew.reloadData()
    }

}
