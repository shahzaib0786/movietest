
import Foundation
import ObjectMapper

public final class  Spoken_languages : Mappable {
	public var iso_639_1 : String?
	public var name : String?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map)
    {

		iso_639_1 <- map["iso_639_1"]
		name <- map["name"]
	}

}
