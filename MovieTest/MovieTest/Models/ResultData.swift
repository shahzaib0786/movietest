//
//  User.swift
//
//  Created by Shahzaib Maqbool on 4/11/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ResultData: Mappable, Codable
{
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys
    {
        static let result = "result"
        static let status_message = "status_message"
        static let status_code = "status_code"
    }
    
    // MARK: Properties
    public var result: [User]?
    public var status_message: String?
    public var status_code: NSInteger?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map)
    {
        result <- map[SerializationKeys.result]
        status_message <- map[SerializationKeys.status_message]
        status_code <- map[SerializationKeys.status_code]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any]
    {
        var dictionary: [String: Any] = [:]
        if let value = result { dictionary[SerializationKeys.result] = value.map{$0.dictionaryRepresentation()} }
        if let value = status_message { dictionary[SerializationKeys.status_message] = value }
        if let value = status_code { dictionary[SerializationKeys.status_code] = value }
        return dictionary
    }
    
}

