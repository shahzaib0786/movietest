
import Foundation
import ObjectMapper

public final class PopularMovies : Mappable {
	public var page : Int?
	public var total_results : Int?
	public var total_pages : Int?
	public var results : [PopularMoviesResult]?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		page <- map["page"]
		total_results <- map["total_results"]
		total_pages <- map["total_pages"]
		results <- map["results"]
	}

}
