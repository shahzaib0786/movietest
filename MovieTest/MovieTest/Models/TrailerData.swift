

import Foundation
import ObjectMapper

public final class TrailerData : Mappable
{
	public var id : Int?
	public var results : [Trailers]?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		id <- map["id"]
		results <- map["results"]
	}

}
