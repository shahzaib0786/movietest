

import Foundation
import ObjectMapper

public final class  Production_companies : Mappable {
	public var id : Int?
	public var logo_path : String?
	public var name : String?
	public var origin_country : String?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		id <- map["id"]
		logo_path <- map["logo_path"]
		name <- map["name"]
		origin_country <- map["origin_country"]
	}

}
