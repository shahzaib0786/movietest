//
//  User.swift
//
//  Created by Shahzaib Maqbool on 4/11/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class User: Mappable, Codable
{
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys
    {
        static let reg_lname = "reg_lname"
        static let login = "login"
        static let user_id = "user_id"
        static let adm = "adm"
        static let email = "reg_email"
        static let reg_fname = "reg_fname"
        static let userName = "reg_username"
        static let reg_phone = "reg_phone"
        static let reg_city = "reg_city"
        static let reg_usertype = "reg_usertype"
        
        
        static let mobile_verified = "mobile_verified"
        static let dob = "dob"
        static let newsletter = "newsletter"
        static let gender = "gender"
        
        
    }
    
    // MARK: Properties
    public var reg_lname: String?
    public var login: String?
    public var user_id: String?
    public var adm: String?
    public var email: String?
    public var reg_fname: String?
    public var reg_city: String?
    public var userName: String?
    public var reg_phone: String?
    public var reg_usertype: String?
    public var requestSent: Bool?
    
    public var mobile_verified: String?
    public var dob: String?
    public var newsletter: String?
    public var gender: Bool?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map)
    {
        reg_lname <- map[SerializationKeys.reg_lname]
        login <- map[SerializationKeys.login]
        reg_city <- map[SerializationKeys.reg_city]
        user_id <- map[SerializationKeys.user_id]
        adm <- map[SerializationKeys.adm]
        email <- map[SerializationKeys.email]
        reg_fname <- map[SerializationKeys.reg_fname]
        userName <- map[SerializationKeys.userName]
        reg_phone <- map[SerializationKeys.reg_phone]
        reg_usertype <- map[SerializationKeys.reg_usertype]
        
        
        mobile_verified <- map[SerializationKeys.mobile_verified]
        dob <- map[SerializationKeys.dob]
        newsletter <- map[SerializationKeys.newsletter]
        gender <- map[SerializationKeys.gender]
        
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any]
    {
        var dictionary: [String: Any] = [:]
        if let value = reg_lname { dictionary[SerializationKeys.reg_lname] = value }
        if let value = login { dictionary[SerializationKeys.login] = value }
        if let value = user_id { dictionary[SerializationKeys.user_id] = value }
        if let value = adm { dictionary[SerializationKeys.adm] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = reg_fname { dictionary[SerializationKeys.reg_fname] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = reg_city { dictionary[SerializationKeys.reg_city] = value }
        if let value = reg_phone { dictionary[SerializationKeys.reg_phone] = value }
        if let value = reg_usertype { dictionary[SerializationKeys.reg_usertype] = value }
        
        return dictionary
    }
    
}

extension User: Equatable {
    public static func == (lhs: User, rhs: User) -> Bool {
        return lhs.user_id == rhs.user_id
    }
}
