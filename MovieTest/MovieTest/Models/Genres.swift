

import Foundation
import ObjectMapper

public final class  Genres : Mappable {
	public var id : Int?
	public var name : String?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
	}

}
