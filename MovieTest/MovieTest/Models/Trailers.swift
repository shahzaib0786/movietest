
import Foundation
import ObjectMapper

public final class Trailers : Mappable {
	public var id : String?
	public var iso_639_1 : String?
	public var iso_3166_1 : String?
	public var key : String?
	public var name : String?
	public var site : String?
	public var size : Int?
	public var type : String?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		id <- map["id"]
		iso_639_1 <- map["iso_639_1"]
		iso_3166_1 <- map["iso_3166_1"]
		key <- map["key"]
		name <- map["name"]
		site <- map["site"]
		size <- map["size"]
		type <- map["type"]
	}

}
