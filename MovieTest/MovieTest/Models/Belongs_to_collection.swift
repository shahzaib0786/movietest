

import Foundation
import ObjectMapper

public final class  Belongs_to_collection : Mappable {
	public var id : Int?
	public var name : String?
	public var poster_path : String?
	public var backdrop_path : String?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		poster_path <- map["poster_path"]
		backdrop_path <- map["backdrop_path"]
	}

}
