
import Foundation
import ObjectMapper

public final class  MovieDetail : Mappable {
	public var adult : Bool?
	public var backdrop_path : String?
	public var belongs_to_collection : Belongs_to_collection?
	public var budget : Int?
	public var genres : [Genres]?
	public var homepage : String?
	public var id : Int?
	public var imdb_id : String?
	public var original_language : String?
	public var original_title : String?
	public var overview : String?
	public var popularity : Double?
	public var poster_path : String?
	public var production_companies : [Production_companies]?
	public var production_countries : [Production_countries]?
	public var release_date : String?
	public var revenue : Int?
	public var runtime : Int?
	public var spoken_languages : [Spoken_languages]?
	public var status : String?
	public var tagline : String?
	public var title : String?
	public var video : Bool?
	public var vote_average : Double?
	public var vote_count : Int?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		adult <- map["adult"]
		backdrop_path <- map["backdrop_path"]
		belongs_to_collection <- map["belongs_to_collection"]
		budget <- map["budget"]
		genres <- map["genres"]
		homepage <- map["homepage"]
		id <- map["id"]
		imdb_id <- map["imdb_id"]
		original_language <- map["original_language"]
		original_title <- map["original_title"]
		overview <- map["overview"]
		popularity <- map["popularity"]
		poster_path <- map["poster_path"]
		production_companies <- map["production_companies"]
		production_countries <- map["production_countries"]
		release_date <- map["release_date"]
		revenue <- map["revenue"]
		runtime <- map["runtime"]
		spoken_languages <- map["spoken_languages"]
		status <- map["status"]
		tagline <- map["tagline"]
		title <- map["title"]
		video <- map["video"]
		vote_average <- map["vote_average"]
		vote_count <- map["vote_count"]
	}

}
