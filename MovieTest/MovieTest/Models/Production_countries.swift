

import Foundation
import ObjectMapper

public final class  Production_countries : Mappable {
	public var iso_3166_1 : String?
	public var name : String?

    public required init?(map: Map)
    {
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {

		iso_3166_1 <- map["iso_3166_1"]
		name <- map["name"]
	}

}
