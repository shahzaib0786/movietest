//
//  Config.swift
//  Motors City
//
//  Created by Shahzaib Maqbool on 26/11/2018.
//  Copyright © 2018 Shahzaib Maqbool. All rights reserved.
//

import Foundation
import UIKit
import SwiftHEXColors
class Config
{
    // MARK: List of Constants
    static let AppLanguages = "AppLanguage"
    
    static let API_KEY = "68f12bb6433ef2c75a0d9fcb9f69eea9"
    static let API_URL = "https://api.themoviedb.org/3/movie"
    static let IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w342"
    
    static let defaults = UserDefaults.standard
    static let AppDelObj = UIApplication.shared.delegate as! AppDelegate
    
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    
    static let isIphone_X = SCREEN_HEIGHT == 812 ? true : false
 
    
    
}
extension UIColor
{
    
    struct MotorsCity
    {
        static let dividerColor = UIColor(hexString: "#e1e1e1")!
        static let blueThemeColor = UIColor(hexString: "#366498")!
        static let orangeThemeColor = UIColor(hexString: "#ED5C01")!
        static let grayLightColor = UIColor(hexString: "#7E7E7E")!
        static let VerySoftGrayColor = UIColor(hexString: "#f9f9f9")!
       
        
        
    }
    
}
