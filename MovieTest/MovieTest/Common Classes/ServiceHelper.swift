//
//  ServiceHelper.swift
//  Al Fana - School
//
//  Created by Shahzaib Maqbool on 09/09/2017.
//  Copyright © 2017 Shahzaib Maqbool. All rights reserved.
//

import UIKit
//import KRProgressHUD
import Foundation
import Alamofire
import CryptoSwift
import SwiftyJSON
let baseURLString = "https://api.motorscity.com/"
//struct Country {
//    var name:String = ""
//}
/////optional
@objc protocol ServiceHelperProtocol:NSObjectProtocol
{


    
}
class ServiceHelper: UIViewController,NSURLConnectionDelegate,XMLParserDelegate
{
    internal typealias errorClosure = ((Error) -> ())?
    //@property (nonatomic, weak) id<ServiceHelperProtocol> delegate;
    weak var delegate: ServiceHelperProtocol?
    var dict = NSArray()
    var mutableData = NSMutableData()
    var currentElementName = NSString()
    var mainString = NSString()
    public var parser = XMLParser()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        parser.delegate = self
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]?
    {
        if let data = text.data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } 
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    ///////FOr Getting Specific/Selected Movie's Trailer links
    public func getTrailerForPopularMovie(movieId:String, success: @escaping (TrailerData) -> (), failure: errorClosure)
    { Alamofire.request("\(Config.API_URL)/\(movieId)/videos?api_key=\(Config.API_KEY)", method: .get, parameters: nil, headers: nil) .responseJSON
            { response in
                switch response.result
                {
                case .success(_):
                    //                    let json = response.result.value
                    //                    print("JSON: \(json ?? "errr")")
                    MapHelper.objectResponseCallback(TrailerData.self, response: response, success: { (result) in
                        success(result)
                        
                    }, failure: failure)
                    //print(" success response is \(response)");
                    
                case .failure(let error):
                    failure?(error)
                    // print("failure response is \(response)");
                }
                
                
        }
        
        
    }
    ///////FOr Getting Specific/Selected Movie's data
    public func getdetailfForPopularMovie(movieId:String, success: @escaping (MovieDetail) -> (), failure: errorClosure)
    {
        
        Alamofire.request("\(Config.API_URL)/\(movieId)?api_key=\(Config.API_KEY)", method: .get, parameters: nil, headers: nil) .responseJSON
            { response in
                switch response.result
                {
                case .success(_):
                    //                    let json = response.result.value
                    //                    print("JSON: \(json ?? "errr")")
                    MapHelper.objectResponseCallback(MovieDetail.self, response: response, success: { (result) in
                        success(result)
                        
                    }, failure: failure)
                    //print(" success response is \(response)");
                    
                case .failure(let error):
                    failure?(error)
                    // print("failure response is \(response)");
                }
                
                
        }
        
        
    }
    ///////For Getting Popolar Movies Data
    public func getPopularMovies(_ success: @escaping (PopularMovies) -> (), failure: errorClosure)
    {
       
        Alamofire.request("\(Config.API_URL)/popular?api_key=\(Config.API_KEY)", method: .get, parameters: nil, headers: nil) .responseJSON
            { response in
                switch response.result
                {
                case .success(_):
//                    let json = response.result.value
//                    print("JSON: \(json ?? "errr")")
                    MapHelper.objectResponseCallback(PopularMovies.self, response: response, success: { (result) in
                        if result.results!.count > 0
                        {
                            success(result)
                        }
                        else
                        {
                            MapHelper.handleError(response, failure: failure)
                        }
                        
                    }, failure: failure)
                    //print(" success response is \(response)");
                    
                case .failure(let error):
                    failure?(error)
                   // print("failure response is \(response)");
                }
                
                
        }
        
        
    }
   

     public func loginUserWithEmail(email:String,password:String, success: @escaping (ResultData) -> (), failure: errorClosure)
    {
        let parameters: Parameters = [
            "email": email,"password": password
        ]
        let headers = [
            "MCA-API-KEY": "B9b\"{^<`a3a980cc6b3b072d7bf867b36aa94d82",
            //"Content-Type": "application/x-www-form-urlencoded"
        ]
        Alamofire.request("\(Config.API_URL)user/get_user", method: .post, parameters: parameters, headers: headers) .responseJSON
            { response in
            switch response.result
            {
            case .success(_):
                let json = response.result.value
                print("JSON: \(json ?? "errr")")
                MapHelper.objectResponseCallback(ResultData.self, response: response, success: { (result) in
                    if result.status_code == 1
                    {
                        success(result)
                    }
                    else
                    {
                        MapHelper.handleError(response, failure: failure)
                    }
                    
                }, failure: failure)
                print(" success response is \(response)");
                
            case .failure(let error):
                failure?(error)
                print("failure response is \(response)");
                }
                
            
        }
        
        
    }
    
    public func signUpUserWithEmail(email:String,password:String, fname:String, lname:String, phone:String, newsletter:String, city:String,  success: @escaping (ResultData) -> (), failure: errorClosure)
    {
        let parameters: Parameters = [
            "email": email,
            "password": password,
            "fname": fname,
            "lname": lname,
            "phone": phone,
            "accept": "true",
            "newsletter": newsletter,
            "city": city,
            "usertype": "3",
            //"lang": "\(AppSettings().getCurrentLanguage() ?? "en")"
        ]
        
        let headers = [
            "MCA-API-KEY": "B9b\"{^<`a3a980cc6b3b072d7bf867b36aa94d82",
        ]
        Alamofire.request("\(Config.API_URL)user/create_user", method: .post, parameters: parameters, headers: headers) .responseJSON
            { response in
                switch response.result
                {
                case .success(_):
                    let json = response.result.value
                    print("JSON: \(json ?? "errr")")
                    
                    MapHelper.objectResponseCallback(ResultData.self, response: response, success: { (result) in
                        if result.status_code == 1
                        {
                            success(result)
                        }
                        else
                        {
                            MapHelper.handleError(response, failure: failure)
                        }
                        
                    }, failure: failure)
                    print(" success response is \(response)");
                    
                case .failure(let error):
                    failure?(error)
                    print("failure response is \(response)");
                }
                
                
        }
        
        
    }
   
 ///////final classs end
}
