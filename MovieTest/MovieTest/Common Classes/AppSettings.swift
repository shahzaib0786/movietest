//
//  AppSettings.swift
//  Al Fana - School
//
//  Created by Shahzaib Maqbool on 12/09/2017.
//  Copyright © 2017 Shahzaib Maqbool. All rights reserved.
//

import UIKit

class AppSettings: NSObject
{

    
    func setUserId(gender:String) -> Void
    {
        UserDefaults().setValue(gender, forKey: "userid")
        UserDefaults().synchronize()
    }
    func getUserId() -> String?
    {
        var txt:String?=""
        txt = UserDefaults.standard.value(forKey:  "userid") as? String

        return txt;
    }
    
    
    
    func setIsUserLogedin(logedin:Bool) -> Void
    {
        UserDefaults().set(logedin, forKey: "isLogedInUser")
        UserDefaults().synchronize()
    }
    func getIsUserLogedin() -> Bool
    {
        var logedin = Bool()

            logedin = UserDefaults.standard.bool(forKey: "isLogedInUser")

        return logedin;
    }
  
    func showSpinner()
    {
        let spinner = BackBoneViewController()
        spinner.show()
    }
    func dismissSpinner()
    {
        let spinner = BackBoneViewController()
        spinner.dissmissSpinner()
    }
    
 


}
