//
//  BaseViewController.swift
//  Motors City
//
//  Created by Shahzaib Maqbool on 05/12/2018.
//  Copyright © 2018 Shahzaib Maqbool. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController
{

    @IBOutlet weak var backBtn: UIButton?
    @IBOutlet weak var navigationVIew: UIView?
    @IBOutlet weak var roundedBtnBlue: UIButton?
    @IBAction func backBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        //navigationController?.popToRootViewController(animated: true)
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        BackBoneViewController.addShadow(view: navigationVIew, color: UIColor.MotorsCity.grayLightColor, width: 3.0)
        BackBoneViewController.petrichors_setBorderRoundedAndShadowFOrButtons(txt: roundedBtnBlue)
//        if BackBoneViewController.isArabicLanguage()
//        {
//            backBtn?.setImage(#imageLiteral(resourceName: "backRight-white"), for: .normal)
//        }
//        else
//        {
//            backBtn?.setImage(#imageLiteral(resourceName: "backLeft-white"), for: .normal)
//        }

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
