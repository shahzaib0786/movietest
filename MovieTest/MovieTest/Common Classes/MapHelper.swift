
import Alamofire
import ObjectMapper
import SwiftyJSON

typealias ErrorClosure = ((Error) -> ())?
private let pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@" +
    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\." +
"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"
class MapHelper {
    
    // MARK: - Handler
    
    static func arrayHandler<T: Mappable>(_ type: T.Type, context: MapContext?, result: Any?) -> (() -> AnyObject?) {
        return {
            guard let result = result else { return nil }
            let mapper = Mapper<T>(context: context).mapArray(JSONObject: result)
            return mapper as AnyObject
        }
    }
    
    static func objectHandler<T: Mappable>(_ type: T.Type, context: MapContext?, result: Any?) -> (() -> AnyObject?) {
        return {
            guard let result = result else { return nil }
            let mapper = Mapper<T>(context: context).map(JSONObject: result)
            return mapper as AnyObject
        }
    }
    
    static func objectResponseCallback<T: Mappable>(_ type: T.Type, context: MapContext? = nil, response: DataResponse<Any>, success: ((T) -> Void), failure: ErrorClosure) {
        let handler = self.objectHandler(type, context: context, result: response.result.value )
        self.handleResponse(response: response, parseHandler: handler, success: success, failure: failure)
    }
    
    static func arrayResponseCallback<T: Mappable>(_ type: T.Type, context: MapContext? = nil, response: DataResponse<Any>, success: (([T]) -> Void), failure: ErrorClosure) {
        let handler = self.arrayHandler(type, context: context, result: response.result.value)
        self.handleResponse(response: response, parseHandler: handler, success: success, failure: failure)
    }
    
    static func objectResponseCallbackWithKey<T: Mappable>(_ type: T.Type, key: String? = nil, context: MapContext? = nil, response: DataResponse<Any>, success: ((T) -> Void), failure: ErrorClosure) {
        var JSON = response.result.value as? [String:Any]
        if let key = key,
            let data = JSON?[key] as? [String:Any]
        {
            JSON = data
        }
        guard let result = JSON else { return }
        let handler = self.objectHandler(type, context: context, result: result)
        self.handleResponse(response: response, parseHandler: handler, success: success, failure: failure)
    }
    
    static func arrayResponseCallbackWithKey<T: Mappable>(_ type: T.Type, key: String? = nil, context: MapContext? = nil, response: DataResponse<Any>, success: (([T]) -> Void), failure: ErrorClosure) {
        var JSON = response.result.value as? [[String:Any]]
        if let key = key,
            let data = (response.result.value as? [String:Any])?[key] as? [[String:Any]] {
            JSON = data
        }
        guard let result = JSON else { return }
        let handler = self.arrayHandler(type, context: context, result: result)
        self.handleResponse(response: response, parseHandler: handler, success: success, failure: failure)
    }
    
    // MARK: - Response
    
    static func handleResponse<T>(_ functionName: String = #function,
                                  response: DataResponse<Any>,
                                  parseHandler: (() -> AnyObject?),
                                  success: ((T) -> Void),
                                  failure: ErrorClosure)
    {
        
        if let result = parseHandler(), response.result.isSuccess
        {
            success(result as! T)
            return
        }
        self.handleError(response, failure: failure)
    }
    
    static func handleError(_ response: DataResponse<Any>, failure: ErrorClosure)
    {
        //let errorMessage = response.value.debugDescription
        var message: String = ""
        if let json = response.result.value
        {
            let dic = JSON(json)
            message = dic["status_message"].stringValue
        }
        else if response.response?.statusCode == nil
        {
            message = response.value.debugDescription
        }
        
        var userInfo = [NSLocalizedDescriptionKey: message]
        if (userInfo.first?.value == "nil")
        {
            userInfo =
                [
                    NSLocalizedDescriptionKey :  "Error".localized,
                    NSLocalizedFailureReasonErrorKey : "Error".localized
            ]
        }
        
        let error = NSError(domain: "Yoospot", code: response.response?.statusCode ?? -1, userInfo: userInfo)
        failure?(error)
    }
}

