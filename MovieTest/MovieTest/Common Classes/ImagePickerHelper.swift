
import Foundation
import UIKit
protocol ImagePickerPresentable: class {
    func showImagePicker()
    func selectedImage(_ image: UIImage?)
}

extension ImagePickerPresentable where Self: UIViewController
{
    
    fileprivate func pickerControllerActionFor(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            let pickerController           = UIImagePickerController()
            pickerController.delegate      = ImagePickerHelper.shared
            pickerController.sourceType    = type
            pickerController.allowsEditing = false
            self.present(pickerController, animated: true)
        }
    }
    
    func showImagePicker() {
        ImagePickerHelper.shared.delegate = self
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.pickerControllerActionFor(for: .camera, title: "Take photo".localized) {
            optionMenu.addAction(action)
        }
        if let action = self.pickerControllerActionFor(for: .photoLibrary, title: "Select from library".localized) {
            optionMenu.addAction(action)
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(optionMenu, animated: true)
    }
}

fileprivate class ImagePickerHelper: NSObject {
    
    weak var delegate: ImagePickerPresentable?
    
    fileprivate struct `Static` {
        fileprivate static var instance: ImagePickerHelper?
    }
    
    fileprivate class var shared: ImagePickerHelper {
        if ImagePickerHelper.Static.instance == nil {
            ImagePickerHelper.Static.instance = ImagePickerHelper()
        }
        return ImagePickerHelper.Static.instance!
    }
    
    fileprivate func dispose() {
        ImagePickerHelper.Static.instance = nil
    }
    
    func picker(picker: UIImagePickerController, selectedImage image: UIImage?) {
        picker.dismiss(animated: true, completion: nil)
        
        self.delegate?.selectedImage(image)
        self.delegate = nil
        self.dispose()
    }
}

extension ImagePickerHelper: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.picker(picker: picker, selectedImage: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return self.picker(picker: picker, selectedImage: nil)
        }
        
        self.picker(picker: picker, selectedImage: image)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//
//    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        self.picker(picker: picker, selectedImage: image)
    }
}

extension ImagePickerHelper: UINavigationControllerDelegate { }
