//
//  Constants.swift
//  CustomFonts
//
//  Created by Attia Mo on 11/12/17.
//  Copyright © 2017 https://Attiamo.me All rights reserved.
//

import Foundation
import UIKit
struct Constants
{
    
    // APPLICATION
    struct Arabic {
        // Font
        static var regularFont = "Cairo-Regular"
        static var boldFont = "Cairo-Bold"
        }
    struct English {
        // Font
        static let regularFont = "Roboto-Regular"
        static let boldFont = "Roboto-Bold"
    }
    struct App {
        // Font
        static var regularFont = "Roboto"
        static var boldFont = "Roboto"

    }
//    struct App {
//        // Font
//        static var regularFont = "AppRegularFont".localized //NSLocalizedString("AppRegularFont", comment: "")
//        static var boldFont = "AppBoldFont".localized//NSLocalizedString("AppBoldFont", comment: "")
//    }
}
