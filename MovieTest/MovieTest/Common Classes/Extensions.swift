

import Foundation
import ObjectMapper

private let pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@" +
    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\." +
"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

///Enums End/////
extension Double {
    func stringValue() -> String {
        let myString = String(self)
        return myString
    }
    
    func ratingValue() -> String {
        let myString = String(self)
        return myString.replacingOccurrences(of: ".", with: ",")
    }
}
extension String {
    func googlePhotoURL() -> String {
        let string = self //"https://maps.googleapis.com/maps/api/place/photo?maxwidth=500&photoreference=\(self)&key=\(Config.GOOGLE_API)"
        return string
    }
    var localized: String
    {
        return ""//self.l10n()
        //return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
        
    }
    var localizedString: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func validPhoneFormat() -> String {
        let newString = self.replacingOccurrences(of: " ", with: "-", options: .literal, range: nil)
        return newString
    }
    
    func isValidEmail() -> Bool {
        let regex = try? NSRegularExpression(
            pattern: pattern,
            options: []
        )
        
        let range = NSRange.init(location: 0, length: self.count)
        return regex?.firstMatch(in: self, options: [], range: range) != nil
    }
    
    func stringByReplacingFirstOccurrenceOfString(
        target: String, withString replaceString: String) -> String {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }
    
    func attributedStringForPartiallyColoredText(_ textToFind: String, with color: UIColor) -> NSMutableAttributedString {
        let mutableAttributedstring = NSMutableAttributedString(string: self)
        let range = mutableAttributedstring.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range.location != NSNotFound {
            mutableAttributedstring.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
        return mutableAttributedstring
    }
    
    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
}

extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func addShadow() {
        layer.cornerRadius = 2
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0.5, height: 2)
        layer.shadowOpacity = 0.5
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shadowRadius = 1.0
    }
    
    static func createOverlayViewFor(_ superview: UIView, alpha: CGFloat) -> UIView {
        let overlay: UIView = UIView(frame: CGRect(x: 0, y: 0, width: superview.frame.size.width, height: superview.frame.size.height))
        overlay.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: alpha)
        overlay.isUserInteractionEnabled = false
        return overlay
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

//extension UINavigationController {
//    open override var preferredStatusBarStyle: UIStatusBarStyle {
//        return topViewController?.preferredStatusBarStyle ?? .default
//    }
//
//    open override var childViewControllerForStatusBarStyle: UIViewController? {
//        return self.topViewController
//    }
//
//    open override var childViewControllerForStatusBarHidden: UIViewController? {
//        return self.topViewController
//    }
//}

//extension UITabBarController {
//    open override var childViewControllerForStatusBarStyle: UIViewController? {
//        return self.childViewControllers.first
//    }
//
//    open override var childViewControllerForStatusBarHidden: UIViewController? {
//        return self.childViewControllers.first
//    }
//}
//
//extension UISplitViewController {
//    open override var childViewControllerForStatusBarStyle: UIViewController? {
//        return self.childViewControllers.first
//    }
//
//    open override var childViewControllerForStatusBarHidden: UIViewController? {
//        return self.childViewControllers.first
//    }
//}

extension Array where Element: Equatable  {
    mutating func delete(element: Iterator.Element) {
        self = self.filter{$0 != element }
    }
}

extension UIResponder {
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
}

extension UICollectionViewCell {
    
    var collectionView: UICollectionView? {
        return next(UICollectionView.self)
    }
    
    var indexPath: IndexPath? {
        return collectionView?.indexPath(for: self)
    }
}

//extension UIImage {
//    var jpeg: Data? {
//        return  UIImageJPEGRepresentation(self, 1)   // QUALITY min = 0 / max = 1
//    }
//    var png: Data? {
//        return self.pngData()
//    }
//}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
extension UIImage
{
    func changeImageTintCOlor() -> UIImage?
    {
        let tintedImage = self.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        return tintedImage
    }
    func changeImageEnglishToArabic(englishImage: UIImage, arabicImage: UIImage) -> UIImage?
    {
        
        if BackBoneViewController.isArabicLanguage()
        {
            return arabicImage
        }
        else
        {
            return englishImage
        }
        
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resizeImage( targetSize: CGSize) -> UIImage
    {
        
        //        image: UIImage,
        // let image:
        let newRect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height).integral
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // Set the quality level to use when rescaling
        context!.interpolationQuality = CGInterpolationQuality.default
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: targetSize.height)
        
        context!.concatenate(flipVertical)
        // Draw into the context; this scales the image
        context?.draw(self.cgImage!, in: CGRect(x: 0.0,y: 0.0, width: newRect.width, height: newRect.height))
        
        let newImageRef = context!.makeImage()! as CGImage
        let newImage = UIImage(cgImage: newImageRef)
        
        // Get the resized image from the context and a UIImage
        UIGraphicsEndImageContext()
        
        return newImage
        
        //        let size = self.size
        //
        //        let widthRatio  = targetSize.width  / size.width
        //        let heightRatio = targetSize.height / size.height
        //
        //        // Figure out what our orientation is, and use that to form the rectangle
        //        var newSize: CGSize
        //        if(widthRatio > heightRatio) {
        //            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        //        } else {
        //            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        //        }
        //
        //        // This is the rect that we've calculated out and this is what is actually used below
        //        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        //
        //        // Actually do the resizing to the rect using the ImageContext stuff
        //        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        //        self.draw(in: rect)
        //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        //        UIGraphicsEndImageContext()
        //
        //        return newImage!
    }
    // Image extension
    func updateImageOrientionUpSide() -> UIImage? {
        if self.imageOrientation == .up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        }
        UIGraphicsEndImageContext()
        return nil
    }
}
public extension UIButton
{
    
    func alignTextBelow(spacing: CGFloat = 6.0)
    {
        if BackBoneViewController.isArabicLanguage()
        {
            if let image = self.imageView?.image
            {
                let imageSize: CGSize = image.size
                self.titleEdgeInsets = UIEdgeInsets(top: spacing, left:0.0 , bottom: -(imageSize.height), right: -imageSize.width)
                let labelString = NSString(string: self.titleLabel!.text!)
                let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
                self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: -titleSize.width, bottom: 0.0, right:0.0)
            }
        }
        else
        {
            if let image = self.imageView?.image
            {
                let imageSize: CGSize = image.size
                self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
                let labelString = NSString(string: self.titleLabel!.text!)
                let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
                self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
            }
        }
    }
    func centerTextAndImage(spacing: CGFloat)
    {
        if BackBoneViewController.isArabicLanguage()
        {
            let insetAmount = spacing / 2
            imageEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: -insetAmount)
        }
        else
        {
            let insetAmount = spacing / 2
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
        }
        
    }
    //image: UIImage
    func centerTextAndImageOnSides(imagepadding: CGFloat)
    {
        if BackBoneViewController.isArabicLanguage()
        {
           // self.rightImage()
            self.moveImageRIghtTextCenter(imagePadding: imagepadding)
        }
        else
        {
            //self.leftImage()
            self.moveImageLeftTextCenter(imagePadding: imagepadding)
        }
        
    }
//   // image: UIImage, renderMode: UIImage.RenderingMode
//    func leftImage() {
//        //self.setImage(self.im.withRenderingMode(renderMode), for: .normal)
//        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (self.imageView?.image?.size.width)! / 2)
//        self.contentHorizontalAlignment = .left
//        self.imageView?.contentMode = .scaleAspectFit
//    }
//    //image: UIImage, renderMode: UIImage.RenderingMode
//    func rightImage(){
//        //self.setImage(image.withRenderingMode(renderMode), for: .normal)
//        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: (self.imageView?.image?.size.width)! / 2, bottom: 0, right: 0)
//        self.contentHorizontalAlignment = .right
//        self.imageView?.contentMode = .scaleAspectFit
//    }
    func moveImageLeftTextCenter(imagePadding: CGFloat = 30.0){
        guard let imageViewWidth = self.imageView?.frame.width else{return}
        guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
        self.contentHorizontalAlignment = .left
        imageEdgeInsets = UIEdgeInsets(top: 0.0, left: imagePadding - imageViewWidth / 2, bottom: 0.0, right: 0.0)
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: (bounds.width - titleLabelWidth) / 2 - imageViewWidth, bottom: 0.0, right: 0.0)
    }
    func moveImageRIghtTextCenter(imagePadding: CGFloat = 30.0){
        guard let imageViewWidth = self.imageView?.frame.width else{return}
        guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
        self.contentHorizontalAlignment = .right
        imageEdgeInsets = UIEdgeInsets(top: 0.0, left:0.0 , bottom: 0.0, right: imagePadding - imageViewWidth / 2)
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left:0.0 , bottom: 0.0, right:(bounds.width - titleLabelWidth) / 2 - imageViewWidth)
    }
    func changeButtonImageTintColor(origImage: UIImage?,imageColor: UIColor)
    {
        let tintedImage = origImage?.changeImageTintCOlor();
        self.setImage(tintedImage, for: .normal)
        self.tintColor = imageColor
    }
    
}
extension String {
    func strikeThrough() -> NSAttributedString
    {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.MotorsCity.orangeThemeColor, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
}
extension UIScrollView
{
    
    func resizeScrollViewContentSize()
    {
        var contentRect = CGRect.zero
        for view in self.subviews
        {
            
            contentRect = contentRect.union(view.frame)
            
        }
        self.contentSize = contentRect.size
    }
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
    
}

extension UIViewController
{
    func add(_ parent: UIViewController)
    {
        parent.addChild(self)
        parent.view.addSubview(view)
        didMove(toParent: parent)
    }
    
    func remove()
    {
        guard parent != nil else { return }
        
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}
extension Formatter
{
    static let withSeparator: NumberFormatter =
    {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
        formatter.numberStyle = .decimal
        return formatter
    }()
}

//extension BinaryInteger {
//    var formattedWithSeparator: String {
//        return Formatter.withSeparator.string(for: self) ?? ""
//    }
//}
extension String {
    var formattedWithSeparator: String {
        //return Formatter.withSeparator.string(for: self) ?? ""
        //let numberFormatter = NumberFormatter()
       // numberFormatter.numberStyle = NumberFormatter.Style.decimal
       // let formattedNumber = numberFormatter.string(for: self)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSize = 3
        numberFormatter.secondaryGroupingSize = 3
        let formattedNumber = numberFormatter.string(from: Int(self)! as NSNumber)
        return "\(formattedNumber ?? "0")"
    }
}
extension Dictionary where Key == String, Value == Array<String> {
    
    mutating func append(_ string:String, key:String) {
        
        if var value = self[key] {
            
            // if an array exist, append to it
            
            value.append(string)
            
            self[key] = value
            
        } else {
            
            // create a new array since there is nothing here
            
            self[key] = [string]
        }
    }
}
/////////font style change
struct Resources {
    
    struct Fonts {
        //struct is extended in Fonts
    }
}

extension Resources.Fonts {
    
    enum Weight: String {
        case light = "Cairo-Regular"
        case regular = "Cairo-Bold"
        case semibold = "Cairo-SemiBold"
        case italic = "Cairo-Light"
    }
}

extension UIFontDescriptor.AttributeName {
    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}

extension UIFont {
    
    @objc class func mySystemFont(ofSize: CGFloat, weight: UIFont.Weight) -> UIFont {
        switch weight {
        case .semibold, .bold, .heavy, .black:
            return UIFont(name: Resources.Fonts.Weight.semibold.rawValue, size: ofSize)!
            
        case .medium, .regular:
            return UIFont(name: Resources.Fonts.Weight.regular.rawValue, size: ofSize)!
            
        default:
            return UIFont(name: Resources.Fonts.Weight.light.rawValue, size: ofSize)!
        }
    }
    
    @objc class func mySystemFont(ofSize size: CGFloat) -> UIFont
    {
        return UIFont(name: Resources.Fonts.Weight.light.rawValue, size: size)!
    }
    
    @objc class func myBoldSystemFont(ofSize size: CGFloat) -> UIFont
    {
        return UIFont(name: Resources.Fonts.Weight.semibold.rawValue, size: size)!
    }
    
    @objc class func myItalicSystemFont(ofSize size: CGFloat) -> UIFont
    {
        return UIFont(name: Resources.Fonts.Weight.italic.rawValue, size: size)!
    }
    
    @objc convenience init(myCoder aDecoder: NSCoder)
    {
        guard
            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
                self.init(myCoder: aDecoder)
                return
        }
        var fontName = ""
        switch fontAttribute {
        case "CTFontRegularUsage", "CTFontMediumUsage":
            fontName = Resources.Fonts.Weight.regular.rawValue
        case "CTFontEmphasizedUsage", "CTFontBoldUsage", "CTFontSemiboldUsage","CTFontHeavyUsage", "CTFontBlackUsage":
            fontName = Resources.Fonts.Weight.semibold.rawValue
        case "CTFontObliqueUsage":
            fontName = Resources.Fonts.Weight.italic.rawValue
        default:
            fontName = Resources.Fonts.Weight.light.rawValue
        }
        self.init(name: fontName, size: fontDescriptor.pointSize)!
    }
    
    class func overrideDefaultTypography() {
        guard self == UIFont.self else { return }
        
        if let systemFontMethodWithWeight = class_getClassMethod(self, #selector(systemFont(ofSize: weight:))),
            let mySystemFontMethodWithWeight = class_getClassMethod(self, #selector(mySystemFont(ofSize: weight:))) {
            method_exchangeImplementations(systemFontMethodWithWeight, mySystemFontMethodWithWeight)
        }
        
        if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))),
            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFont(ofSize:))) {
            method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
        }
        
        if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))),
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFont(ofSize:))) {
            method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
        }
        
        if let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:))),
            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFont(ofSize:))) {
            method_exchangeImplementations(italicSystemFontMethod, myItalicSystemFontMethod)
        }
        
        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))),
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
        }
    }
}
///////for font change of whole app
//extension UILabel {
//    
//    @objc var substituteFontName : String
//        {
//        get { return self.font.fontName }
//        set {
//            if self.font.fontName.range(of:"-Bd") == nil
//            {
//                self.font = UIFont(name: Constants.App.boldFont , size: self.font.pointSize)
//            }
//            else
//            {
//                self.font = UIFont(name: newValue, size: self.font.pointSize)
//            }
//        }
//    }
//    
//    @objc var substituteFontNameBold : String {
//        get { return self.font.fontName }
//        set {
//            if self.font.fontName.range(of:"-Bd") != nil {
//                self.font = UIFont(name: newValue, size: self.font.pointSize)
//            }
//        }
//    }
//}
//extension UITextField
//{
//    @objc var substituteFontName : String
//        {
//        get { return self.font!.fontName }
//        set { self.font = UIFont(name: newValue, size: (self.font?.pointSize)!) }
//    }
//}

extension UIFont
{
    class func appRegularFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: Constants.App.regularFont, size: size)!
    }
    
    class func appBoldFontWith( size:CGFloat ) -> UIFont{
        return  UIFont(name: Constants.App.boldFont, size: size)!
    }
}
extension UILabel {
    @objc public var substituteFontName : String {
        get {
            return self.font.fontName;
        }
        set {
            let fontNameToTest = self.font.fontName.lowercased();
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            }
            else if fontNameToTest.range(of: "sfuitext") != nil {
                fontName += "-Regular";
            }
            else if fontNameToTest.range(of: "regular") != nil {
                fontName += "-Regular";
            }
            else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName.capitalized, size: self.font.pointSize)
            self.layoutIfNeeded()
            self.layoutSubviews()
            UIView().layoutIfNeeded()
            UIView().layoutSubviews()
        }
    }
}
extension UIButton
{
    @objc public var substituteFontName : String
        {
        get
        {
            
            return self.titleLabel!.font.fontName;
        }
        set
        {
            
            
            let fontNameToTest = self.titleLabel?.font?.fontName.lowercased();
            //print("font isss \(fontNameToTest)")
            var fontName = newValue;
            if fontNameToTest?.range(of: "bold") != nil
            {
                fontName += "-Bold";
            }
            else if fontNameToTest?.range(of: "medium") != nil
            {
                fontName += "-Medium";
            }
            else if fontNameToTest?.range(of: "sfuitext") != nil {
                fontName += "-Regular";
            }
            else if fontNameToTest?.range(of: "regular") != nil {
                fontName += "-Regular";
            }
            else if fontNameToTest?.range(of: "light") != nil
            {
                fontName += "-Light";
            }
            else if fontNameToTest?.range(of: "ultralight") != nil
            {
                fontName += "-UltraLight";
            }
            print("font isss2222 \(fontName.capitalized)")
            self.titleLabel?.font = UIFont(name: fontName.capitalized, size: (self.titleLabel?.font.pointSize)!)
            
            self.titleLabel?.minimumScaleFactor = 0.1
            self.titleLabel?.numberOfLines = 0
            self.titleLabel?.adjustsFontSizeToFitWidth = true
            self.titleLabel?.layoutIfNeeded()
            self.titleLabel?.layoutSubviews()
            UIView().layoutIfNeeded()
            UIView().layoutSubviews()
            
        }
    }
}
extension UITextView
{
    @objc public var substituteFontName : String
        {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
}

extension UITextField {
    @objc public var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
