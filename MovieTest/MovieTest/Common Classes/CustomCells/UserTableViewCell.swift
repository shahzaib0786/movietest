//
//  UserTableViewCell.swift
//  Yoospot
//
//  Created by Shahzaib Maqbool on 4/27/18.
//  Copyright © 2018 Shahzaib Maqbool. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell
{

    @IBOutlet weak var sarLbl: UILabel!
    @IBOutlet weak var mainVIewCOnstrntHeight: NSLayoutConstraint!
    @IBOutlet weak var engineType: UILabel?
    @IBOutlet weak var maualLbl: UILabel?
    @IBOutlet weak var profileImageView: UIImageView?
    @IBOutlet weak var fullNameLabel: UILabel?
    @IBOutlet weak var emailLabel: UILabel?
    @IBOutlet weak var mainView: UIView?
    @IBOutlet weak var termLbl: UILabel!
    @IBOutlet weak var installmentLbl: UILabel!
    @IBOutlet weak var downPaymnetLbl: UILabel!
    
    @IBOutlet weak var messageIconBtn: SpecialButton!
    @IBOutlet weak var adIdLbl: UILabel!
    @IBOutlet weak var coloredView: UIView!
    @IBOutlet weak var interestLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel?
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var financeLbl: UILabel!
    @IBOutlet weak var financeStrLbl: UILabel!
    @IBOutlet weak var carTypeLbl: UILabel!
    @IBOutlet weak var kilometersLbl: UILabel!
    @IBOutlet weak var tagViewHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var specialCallImageVIew: UIImageView!
    
    @IBOutlet weak var spearatorView: UIView!
    
    @IBOutlet weak var oldPriceHeightConstrnt: NSLayoutConstraint!
    @IBOutlet weak var priceHeightConstrnt: NSLayoutConstraint!
    
    @IBOutlet weak var buttonsTagView: UIView!
    @IBOutlet weak var image2: UIImageView?
    
    @IBOutlet weak var mainBtn1: UIButton!
    @IBOutlet weak var sideVIew: UIView!
    @IBOutlet weak var descLbl2: UILabel?
    @IBOutlet weak var image3: UIImageView?
    
    @IBOutlet weak var resultVIewsLbl: UILabel!
    @IBOutlet weak var detailViewsLbl: UILabel!
    @IBOutlet weak var adStatusLbl: UILabel!
    
  
    @IBOutlet weak var descLbl3: UILabel?
    @IBOutlet weak var image4: UIImageView?
    
    @IBOutlet weak var btnMain: SpecialButton!
    @IBOutlet weak var mainBtn2: UIButton!
    @IBOutlet weak var mainBtn3: UIButton!
    @IBOutlet weak var mainVIew2: UIView!
    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var carTagImageVIew: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var notRepliedBtn: SpecialButton!
    
    @IBOutlet weak var replyBtn: SpecialButton!
    @IBOutlet weak var viewReplyBtn: SpecialButton!
    @IBOutlet weak var unreadBtn: SpecialButton!
    @IBOutlet weak var dltBtn: SpecialButton!
    
    @IBOutlet weak var discountView: UIView!
    
    @IBOutlet weak var discountViewLbl: UIButton!
    @IBOutlet weak var discountExpiryLbl: UILabel!
      @IBOutlet weak var discountHeightCOnsrnt: NSLayoutConstraint!
    @IBOutlet weak var alertBtn: SpecialButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
//        profileImageView?.layer.cornerRadius = (profileImageView?.frame.size.height)! / 2
//        profileImageView?.layer.masksToBounds = true
//         
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                image2!.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                image2!.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    func setCustomImage(image : UIImage) {
        
        let aspect = image.size.width / image.size.height
        
        let constraint = NSLayoutConstraint(item: image2!, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: image2!, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        
        aspectConstraint = constraint
        
        image2!.image = image
    }
    
//    func configureCell(_ user: User)
//    {
//        fullNameLabel.text = user.fullName
//        emailLabel.text = user.nameId
// 
//        if let urlString = user.picture
//        {
//            profileImageView.sd_setImage(with: URL(string: urlString), completed: nil)
//        }
//        else
//        {
//            profileImageView.image = #imageLiteral(resourceName: "profilePlaceholder")
//        }
//    }
}

