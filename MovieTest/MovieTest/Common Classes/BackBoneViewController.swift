//
//  BackBoneViewController.swift
//  Motors City
//
//  Created by Shahzaib Maqbool on 29/08/2017.
//  Copyright © 2017 Shahzaib Maqbool. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftMessages
import SVProgressHUD
public enum UIButtonBorderSide
{
    case Top, Bottom, Left, Right
}

class BackBoneViewController: UIViewController
{

   
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //////For setting cornor radius and Rounding//////
    class func petrichors_setCornerRadiusWithBtn(btn:UIView,radius:CGFloat) -> Void
    {
        btn.layer.cornerRadius = radius;
        btn.layer.masksToBounds = true;
    }

    //////For setting border color and Width//////
    class func petrichors_setborderColorWithBtn(btn:UIView?,color:CGColor,borderWidth:CGFloat) -> Void
    {
        btn?.layer.borderColor = color; //[UIColor redColor].CGColor;
        btn?.layer.borderWidth = borderWidth; //2.0f;
        
    }
    class func petrichors_setCornerRadiusWith(side: UIButtonBorderSide,btn:UIView?,radius:CGFloat) -> Void
    {
        
        switch side
        {
        case .Right:
            btn?.layer.cornerRadius = radius;
            btn?.layer.masksToBounds = true; //2.0f;
            if #available(iOS 11.0, *) {
                btn?.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            } else {
                // Fallback on earlier versions
            }
        case .Left:
            btn?.layer.cornerRadius = radius;
            btn?.layer.masksToBounds = true;
            if #available(iOS 11.0, *) {
                btn?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            } else {
                // Fallback on earlier versions
            }
        case .Top:
            break
        case .Bottom:
            break
        }
    }
    //////For setting border color and Width//////
    class func petrichors_setBorderRoundedAndShadow(txt:UIView?) -> Void
    {
        txt?.layer.backgroundColor = UIColor.white.cgColor
        txt?.layer.borderColor = UIColor.gray.cgColor
        txt?.layer.borderWidth = 0.0
        txt?.layer.cornerRadius = 5
        txt?.layer.masksToBounds = false
        txt?.layer.shadowRadius = 2.0
        txt?.layer.shadowColor = UIColor.black.cgColor
        txt?.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
        txt?.layer.shadowOpacity = 1.0
        txt?.layer.shadowRadius = 1.0
    }
    class func petrichors_setBorderRoundedAndShadowFOrButtons(txt:UIView?) -> Void
    {
       // txt.layer.backgroundColor = UIColor.white.cgColor
        txt?.layer.borderColor = UIColor.lightGray.cgColor
        txt?.layer.borderWidth = 0.0
        txt?.layer.cornerRadius = 5
        txt?.layer.masksToBounds = false
        txt?.layer.shadowRadius = 2.0
        txt?.layer.shadowColor = UIColor.lightGray.cgColor
        txt?.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
        txt?.layer.shadowOpacity = 1.0
        txt?.layer.shadowRadius = 1.0
    }
    class func petrichors_setHollowBorderRoundedAndShadow(txt:UIView?) -> Void
    {
        // txt.layer.backgroundColor = UIColor.white.cgColor
        txt?.layer.borderColor = UIColor.MotorsCity.blueThemeColor.cgColor
        txt?.layer.borderWidth = 1.0
        txt?.layer.cornerRadius = 5
        txt?.layer.masksToBounds = false
        txt?.layer.shadowRadius = 2.0
        txt?.layer.shadowColor = UIColor.lightGray.cgColor
        txt?.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
        txt?.layer.shadowOpacity = 1.0
    }
    ///////Rounded Profile Image//////
   class func petrichors_roundedProfileView(view:UIView?) -> Void
    {
        view?.layer.cornerRadius = (view?.frame.size.height)!/2;
    view?.layer.masksToBounds = true;
    }
    class func addBorder(side: UIButtonBorderSide, view: UIView, color: UIColor, width: CGFloat) -> Void{
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        switch side
        {
        case .Top:
            border.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: width)
        case .Bottom:
            border.frame = CGRect(x: 0, y: view.frame.size.height - width, width: view.frame.size.width, height: width)
        case .Left:
            border.frame = CGRect(x: 0, y: 0, width: width, height: view.frame.size.height)
        case .Right:
            border.frame = CGRect(x: view.frame.size.width - width, y: 0, width: width, height: view.frame.size.height)
        }
        
        view.layer.addSublayer(border)
    }
    class func addShadow(view: UIView?, color: UIColor, width: CGFloat) -> Void
    {
        view?.layer.masksToBounds = false
        view?.layer.shadowColor = color.cgColor
        view?.layer.shadowOffset = CGSize.init(width: 1.0, height: width)
        view?.layer.shadowOpacity = 1.0
        view?.layer.shadowRadius = width
        
    }
    class func addDashedBorder(view: UIView?, color: UIColor) -> Void
    {
        view?.layer.borderColor = color.cgColor; //[UIColor redColor].CGColor;
        view?.layer.borderWidth = 1.0; //2.0f;
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = color.cgColor
//        yourViewBorder.lineDashPattern = [12, 12]
//        yourViewBorder.frame = (view?.bounds)!
//        yourViewBorder.fillColor = nil
//        yourViewBorder.lineWidth = 2
//        yourViewBorder.path = UIBezierPath(rect: (view?.bounds)!).cgPath
//        view!.layer.addSublayer(yourViewBorder)
//        view?.layoutIfNeeded()
        
    }
    class func petrichors_showErrorMsgOnStatusBar(title:String, desc:String)
    {
        var config = SwiftMessages.Config()
        
        // Slide up from the bottom.
        config.presentationContext = .window(windowLevel: .statusBar)
        
        // Display in a window at the specified window level: UIWindowLevelStatusBar
        // displays over the status bar while UIWindowLevelNormal displays under.
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        
        // Disable the default auto-hiding behavior.
        config.duration = .forever
        
        // Dim the background like a popover view. Hide when the background is tapped.
        config.dimMode = .gray(interactive: true)
        
        // Disable the interactive pan-to-hide gesture.
        config.interactiveHide = false
        
        // Specify a status bar style to if the message is displayed directly under the status bar.
        config.preferredStatusBarStyle = .lightContent
        
        // Specify one or more event listeners to respond to show and hide events.
        config.eventListeners.append() { event in
            if case .didHide = event { print("yep") }
        }
        
        
        //        let alertView = SCLAlertView()
        //
        //        alertView.showError(title, subTitle: desc, closeButtonTitle: "OK")
        let error = MessageView.viewFromNib(layout: .statusLine)
        error.configureTheme(.error)
        error.configureContent(title: title, body: desc)
        error.button?.isHidden = true
        //error.button?.setTitle("OK", for: .normal)
        // error.buttonTapHandler = { _ in SwiftMessages.hide() }
        //error.buttonTapHandler
        
        
        let warning = MessageView.viewFromNib(layout: .cardView)
        warning.configureTheme(.warning)
        warning.configureDropShadow()
        
        //SwiftMessages.show(view: error)
        SwiftMessages.show(config: config, view: error)
        AppSettings().dismissSpinner()
        //AppSettings().dismissSpinner()
    }
    class func petrichors_showErrorMsgWithTitle(title:String, desc:String)
    {
        var config = SwiftMessages.Config()
        
        // Slide up from the bottom.
        config.presentationContext = .window(windowLevel: .statusBar)
        
        // Display in a window at the specified window level: UIWindowLevelStatusBar
        // displays over the status bar while UIWindowLevelNormal displays under.
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        
        // Disable the default auto-hiding behavior.
        config.duration = .forever
        
        // Dim the background like a popover view. Hide when the background is tapped.
        config.dimMode = .gray(interactive: true)
        
        // Disable the interactive pan-to-hide gesture.
        config.interactiveHide = false
        
        // Specify a status bar style to if the message is displayed directly under the status bar.
        config.preferredStatusBarStyle = .lightContent
        
        // Specify one or more event listeners to respond to show and hide events.
        config.eventListeners.append() { event in
            if case .didHide = event { print("yep") }
        }
        
        
        //        let alertView = SCLAlertView()
        //
        //        alertView.showError(title, subTitle: desc, closeButtonTitle: "OK")
        let error = MessageView.viewFromNib(layout: .statusLine)
        error.configureTheme(.error)
        error.configureContent(body: desc)
        error.button?.isHidden = true
        //error.button?.setTitle("OK", for: .normal)
        // error.buttonTapHandler = { _ in SwiftMessages.hide() }
        //error.buttonTapHandler
        
        
        let warning = MessageView.viewFromNib(layout: .cardView)
        warning.configureTheme(.warning)
        warning.configureDropShadow()
        
        //SwiftMessages.show(view: error)
        SwiftMessages.show(config: config, view: error)
        AppSettings().dismissSpinner()
        //AppSettings().dismissSpinner()
    }

    class func petrichors_showSuccessMsgWithTitle(title:String, desc:String)
    {
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureTheme(.success)
        success.configureDropShadow()
        success.configureContent(title: title, body: desc)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .bottom
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: successConfig, view: success)
        AppSettings().dismissSpinner()
        
        
    }
    class func petrichors_showALertMsgWithTitle(title:String, desc:String)
    {
        let messageView: MessageView = MessageView.viewFromNib(layout: .centeredView)
        messageView.configureBackgroundView(width: 250)
        messageView.configureContent(title: title, body: desc, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Ok".localized) { _ in
            SwiftMessages.hide()
        }
        messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
        messageView.backgroundView.layer.cornerRadius = 10
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .center
        config.duration = .forever
        config.dimMode = .blur(style: .dark, alpha: 1, interactive: true)
        config.presentationContext  = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: config, view: messageView)
        AppSettings().dismissSpinner()
    }

    //////For setting TextField Left align Padding//////
    class func petrichors_setTextFieldLeftPadding(txtField:UITextField) -> Void
    {
        if BackBoneViewController.isArabicLanguage()
        {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            txtField.rightView = paddingView
            txtField.rightViewMode = .always
        }
        else
        {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            txtField.leftView = paddingView
            txtField.leftViewMode = .always
            
        }
    
        
    }
    ///////////////VIew Hide/show with animation//////////////////
    class func petrichors_setViewHideAndShow(view:UIView, hidden:Bool, duration:TimeInterval) -> Void
    {
        UIView.transition(with: view,
                          duration:duration,
                          options:UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {
                            view.isHidden=hidden
                            // do something
        }, completion:{
            finished in
            // do something
        })
   
        
    }
    
    // Screen width.
    class var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    class var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
//    class func changeImageTint(image:UIImage)
//    {
//        image.withRenderingMode(.alwaysTemplate)
//        cell.mainImage?.tintColor = UIColor.MotorsCity.blueThemeColor
//    }
    class func isArabicLanguage() -> Bool
    {
        let defaultLnguage = "\(Config.defaults.value(forKey: "AppLanguage") ?? "en")"
        var isArabic = Bool()
        
        if (defaultLnguage == "ar")
        {
            isArabic = true
        }
        else
        {
            isArabic = false
        }
        return isArabic
    }
   class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    let window = UIApplication.shared.keyWindow!
    func show()
    {
        
        let exampleColor0 : [UIColor] = [UIColor.red,  UIColor.MotorsCity.blueThemeColor,  UIColor.MotorsCity.orangeThemeColor, .purple]
//        let exampleColor1 : [UIColor] = [UIColor.magenta, .red]
//        let exampleColor2 : [UIColor] = [UIColor.red,  .green ]
//        let exampleColor3 : [UIColor] = [UIColor.brown,  .yellow, .cyan]
//        let exampleColor4 : [UIColor] = [UIColor.orange, .purple]
        var animationTypes: [RPLoadingAnimationType] = [
            .rotatingCircle,
            .spininngDot,
            .lineScale,
            .dotTrianglePath,
            .dotSpinningLikeSkype,
            .funnyDotsA
        ]
        
        let size = CGSize(width: 100, height: 100)
        
        let animationFrame = CGRect(origin: CGPoint(x: (Config.SCREEN_WIDTH/2) - 50, y: (Config.SCREEN_HEIGHT/2) - 50), size: size)
        
        //var colors : [UIColor] = [UIColor.black]
        let animationView = RPLoadingAnimationView(
            frame: animationFrame,
            type: animationTypes[2],
            colors: exampleColor0,
            size: size
        )
        //animationView.tag = 1001
        
        let view = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
        view.addSubview(animationView)
        window.addSubview(view);
        window.backgroundColor = UIColor.MotorsCity.grayLightColor
        window.alpha = 1
        //window.tag = 1001
        view.tag = 1001
        animationView.setupAnimation()
        //animationView.dis
    }
    func dissmissSpinner()
    {
        for view in self.window.subviews
        {
            if view.tag == 1001
            {
                view.removeFromSuperview()
            }
        }
        window.backgroundColor = UIColor.clear
        window.alpha = 1
      //  window.viewWithTag(1001)
    }


}

class ButtonWithShadow: UIButton {
    
    override func draw(_ rect: CGRect) {
        updateLayerProperties()
    }
    
    func updateLayerProperties() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 10.0
        self.layer.masksToBounds = false
    }
    
}

class SpecialLabel: UILabel
{
    
    var MainImageView: UIImageView?
    var tintedColor: UIColor?
    @IBInspectable
    public var cornerRadius: CGFloat
    {
        set (radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }
        
        get {
            return self.layer.cornerRadius
        }
    }
 
    
}
class SpecialView: UIView
{
    
    var MainImageView: UIImageView?
    var tintedColor: UIColor?

    @IBInspectable var cornerRadius: CGFloat = 3
        {
        
        didSet
        {
            refreshCorners(value: cornerRadius)
        }
    }
    @IBInspectable var dashedBoderColor: UIColor = UIColor.MotorsCity.orangeThemeColor
        {
        didSet
        {
            addDashedBorder(color: dashedBoderColor)
        }
    }
    @IBInspectable var AddBottomShadowWidth: CGFloat = 3
        {
        didSet
        {
            //refreshCorners(value: cornerRadius)
            addShadow(color: self.tintedColor!, width: AddBottomShadowWidth)
        }
    }
    func refreshCorners(value: CGFloat)
    {
        self.layer.cornerRadius = value
        self.layer.masksToBounds = value > 0
    }
    func addShadow(color: UIColor, width: CGFloat) -> Void
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize.init(width: 1.0, height: width)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = width
        
    }
    func addDashedBorder(color: UIColor) -> Void
    {
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = color.cgColor
        yourViewBorder.lineDashPattern = [12, 12]
        yourViewBorder.frame = (self.bounds)
        yourViewBorder.fillColor = nil
        //yourViewBorder.lineWidth = 2
        yourViewBorder.path = UIBezierPath(rect: (self.bounds)).cgPath
        self.layer.addSublayer(yourViewBorder)
        
    }
    @IBInspectable
    public var borderBottomColor: UIColor
    {
        set (tintColorMain)
        {
            self.tintColor = tintColorMain
            self.tintedColor = tintColorMain
        }
        get
        {
            return self.tintColor
        }
    }
    
}
class SpecialImage: UIImageView
{
    
    var MainImageView: UIImageView?
    var tintedColor: UIColor?
    @IBInspectable
    public var tintColorMain: UIColor
    {
        set (tintColorMain)
        {
            self.image = self.image?.changeImageTintCOlor()
            self.tintColor = tintColorMain
            self.tintedColor = tintColorMain
        }
        
        get
        {
            return self.tintColor
        }
    }
    
}

class SpecialButton: UIButton
{
    
    var Row: Int = 0
    var Section: Int = 0
    var tintedColor: UIColor?
    @IBInspectable var cornerRadius: CGFloat = 3
        {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    @IBInspectable var AddBottomShadowWidth: CGFloat = 3
        {
        
        didSet
        {
           // refreshCorners(value: cornerRadius)
            addShadow(color: UIColor.MotorsCity.grayLightColor, width: AddBottomShadowWidth)
        }
    }
    func refreshCorners(value: CGFloat)
    {
        self.layer.cornerRadius = value
        self.layer.masksToBounds = value > 0
    }
    func addShadow(color: UIColor, width: CGFloat) -> Void
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize.init(width: 1.0, height: width)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = width
        
    }
    @IBInspectable
    public var tintColorMain: UIColor
    {
        set (tintColorMain)
        {
            let tintedImage = self.currentImage?.changeImageTintCOlor()
            self.setImage(tintedImage, for: .normal)
            self.tintColor = tintColorMain
        }
        
        get
        {
            return self.tintColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit()
    {
    
        // Common logic goes here
    }
    
}
class LoadMoreActivityIndicator
{
    
    private let spacingFromLastCell: CGFloat
    private let spacingFromLastCellWhenLoadMoreActionStart: CGFloat
    private weak var activityIndicatorView: UIActivityIndicatorView?
    private weak var scrollView: UIScrollView?
    
    private var defaultY: CGFloat {
        guard let height = scrollView?.contentSize.height else { return 0.0 }
        return height + spacingFromLastCell
    }
    
    init (scrollView: UIScrollView, spacingFromLastCell: CGFloat, spacingFromLastCellWhenLoadMoreActionStart: CGFloat) {
        self.scrollView = scrollView
        self.spacingFromLastCell = spacingFromLastCell
        self.spacingFromLastCellWhenLoadMoreActionStart = spacingFromLastCellWhenLoadMoreActionStart
        let size:CGFloat = 40
        let frame = CGRect(x: (scrollView.frame.width-size)/2, y: scrollView.contentSize.height + spacingFromLastCell, width: size, height: size)
        let activityIndicatorView = UIActivityIndicatorView(frame: frame)
        activityIndicatorView.color = .black
        activityIndicatorView.isHidden = false
        activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
        scrollView.addSubview(activityIndicatorView)
        activityIndicatorView.isHidden = isHidden
        self.activityIndicatorView = activityIndicatorView
    }
    
    private var isHidden: Bool {
        guard let scrollView = scrollView else { return true }
        return scrollView.contentSize.height < scrollView.frame.size.height
    }
    
    func start(closure: ()->()) {
        guard let scrollView = scrollView, let activityIndicatorView = activityIndicatorView else { return }
        let offsetY = scrollView.contentOffset.y
        activityIndicatorView.isHidden = isHidden
        if !isHidden && offsetY >= 0 {
            let contentDelta = scrollView.contentSize.height - scrollView.frame.size.height
            let offsetDelta = offsetY - contentDelta
            
            let newY = defaultY-offsetDelta
            if newY < scrollView.frame.height {
                activityIndicatorView.frame.origin.y = newY
            } else {
                if activityIndicatorView.frame.origin.y != defaultY {
                    activityIndicatorView.frame.origin.y = defaultY
                }
            }
            
            if !activityIndicatorView.isAnimating {
                if offsetY > contentDelta && offsetDelta >= spacingFromLastCellWhenLoadMoreActionStart && !activityIndicatorView.isAnimating {
                    activityIndicatorView.startAnimating()
                    closure()
                }
            }
            
            if scrollView.isDecelerating {
                if activityIndicatorView.isAnimating && scrollView.contentInset.bottom == 0 {
                    UIView.animate(withDuration: 0.3) { [weak self] in
                        if let bottom = self?.spacingFromLastCellWhenLoadMoreActionStart {
                            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
                        }
                    }
                }
            }
        }
    }
    
    func stop() {
        guard let scrollView = scrollView , let activityIndicatorView = activityIndicatorView else { return }
        let contentDelta = scrollView.contentSize.height - scrollView.frame.size.height
        let offsetDelta = scrollView.contentOffset.y - contentDelta
        if offsetDelta >= 0 {
            UIView.animate(withDuration: 0.3) {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true
    }
}
//UITextField : override textRect, editingRect
class LeftPaddedTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
}
