//
//  MovieDetailViewController.swift
//  MovieTest
//
//  Created by Shahzaib Maqbool on 26/04/2019.
//  Copyright © 2019 Shahzaib Maqbool. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage
import AVFoundation
import XCDYouTubeKit
import AVKit
class MovieDetailViewController: UIViewController,AVAudioPlayerDelegate
{
    
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var GenresLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var overviewLbl: UILabel!
    
    var movieId: String?
    let serviceHelper = ServiceHelper()
    var movieDetail: MovieDetail?
    var trailerData: TrailerData?
    var movieTrailers: [Trailers] = []
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        self.scrollVIew.resizeScrollViewContentSize()
    }
    @IBAction func watchTrailerBtnCLicked(_ sender: Any)
    {
        if self.movieTrailers.count > 0
        {
//            let videoURL = URL(string: "\(self.movieDetail?.ur ?? "")")!
//            UIApplication.shared.open(videoURL, options: .init(), completionHandler: nil)
            self.playVideo(videoIdentifier: "\(self.movieTrailers[0].key ?? "dKrVegVI0Us")")
        }
        //...openURL("https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
    }
    struct YouTubeVideoQuality {
        static let hd720 = NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)
        static let medium360 = NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)
        static let small240 = NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)
    }
    
    func playVideo(videoIdentifier: String?) {
        let playerViewController = AVPlayerViewController()
        self.present(playerViewController, animated: true, completion: nil)
        
        XCDYouTubeClient.default().getVideoWithIdentifier(videoIdentifier) { [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
            if let streamURLs = video?.streamURLs, let streamURL = (streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs[YouTubeVideoQuality.hd720] ?? streamURLs[YouTubeVideoQuality.medium360] ?? streamURLs[YouTubeVideoQuality.small240]) {
                playerViewController?.player = AVPlayer(url: streamURL)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    //////to update values in label and image from api
    func createUi()
    {
        let genre: [String] = (self.movieDetail?.genres?.map({$0.name!}))!
        let csvStr: String = genre.joined(separator: ",")
        self.movieTitle.text = "   \(self.movieDetail?.title ?? "")"
        self.GenresLbl.text = "\(csvStr )"
        self.dateLbl.text = "\(self.movieDetail?.release_date ?? "")"
        self.overviewLbl.text = "\(self.movieDetail?.overview ?? "")"
        if self.movieDetail?.backdrop_path != nil
        {
            if let urlString = self.movieDetail?.backdrop_path
            {
                self.posterImage?.sd_setImage(with: URL(string: "\(Config.IMAGE_BASE_URL)\(urlString)"), placeholderImage: UIImage(named: "news_placeholder"))
                
            }
            else
            {
                self.posterImage?.image = #imageLiteral(resourceName: "news_placeholder")
            }
        }
        else
        {
            self.posterImage?.image = #imageLiteral(resourceName: "news_placeholder")
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.getdetailfForPopularMovie()
    }
    
    /////gettting detail of specific movie.
    func getdetailfForPopularMovie()
    {
        if Connectivity.isConnectedToInternet
        {
            AppSettings().showSpinner()
            
            serviceHelper.getdetailfForPopularMovie(movieId: "\(movieId ?? "")", success:{ (movies) in
                print("Success")
                self.movieDetail = movies
                self.createUi()
                self.getTrailerForPopularMovie()
                //AppSettings().dismissSpinner()
                self.view.layoutIfNeeded()
                self.viewWillLayoutSubviews()
                
                
            })
            { (error) in
                BackBoneViewController.petrichors_showErrorMsgOnStatusBar(title: "Error!".localized, desc: "No more results.".localized)
                print("Error")
            }
        }
        else
        {
            BackBoneViewController.petrichors_showErrorMsgWithTitle(title: "Error!".localized, desc: "No network connection".localized)
        }
    }
    /////for getting movie trailers
    func getTrailerForPopularMovie()
    {
        if Connectivity.isConnectedToInternet
        {
            AppSettings().showSpinner()
            
            serviceHelper.getTrailerForPopularMovie(movieId: "\(movieId ?? "")", success:{ (movies) in
                print("Success")
                self.trailerData = movies
                self.movieTrailers = self.trailerData!.results!
                //self.createUi()
                AppSettings().dismissSpinner()
                
                
            })
            { (error) in
                BackBoneViewController.petrichors_showErrorMsgOnStatusBar(title: "Error!".localized, desc: "No more results.".localized)
                print("Error")
            }
        }
        else
        {
            BackBoneViewController.petrichors_showErrorMsgWithTitle(title: "Error!".localized, desc: "No network connection".localized)
        }
    }
    
    
    
    
}
